function each(arr,fn){
	for(var i=0,l=arr.length,t;i<l;i++){
		t=arr[i];
		if(fn(t,i)===false)break;
	}
}
//url参数和json互相转换 转换后存储在req.query属性中
!function(route){
	var to=route.to,use=route.use;
	
	function stringify(obj){
		var a=[];
		for(key in obj){
			a.push(key+'='+obj[key]);
		}
		return a.join('&')
	}
	use(function(req,next){
		var urlp=req.path.split('?')[1];
		if(!urlp){
			next();
			return;
		}
		var a=urlp.split('&');
		var r={};
		each(a,function(t,i){
			var b=t.split('=');
			r[b[0]]=b[1];
		});
		req.query=r;
		next();
	});
	route.mix({
		to:function(path,obj){
			if(path.indexOf('?')==-1&&obj){
				path+='?'+stringify(obj);
			}
			to(window.encodeURI(path));
		}
	})
}(route);